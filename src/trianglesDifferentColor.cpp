#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <math.h>
#include "Shader.h"

//redimension of the window' size
void framebuffer_size_callback(GLFWwindow *window, int width, int height);

//process user input on the window
void processInput(GLFWwindow *window);

int main()
{
    //configure glfw context (version, core profile)
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    //window creation
    GLFWwindow *window = glfwCreateWindow(800, 600, "Madness Car Simulator", NULL, NULL);

    if (window == NULL) {
        std::cout << "failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    //initialize GLAD before calling Opengl functions
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    glViewport(0, 0, 800, 600);

    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    Shader firstTriangleShader("./shaders/firstTriangle.vs", "./shaders/firstTriangle.fs");
    Shader secondTriangleShader("./shaders/secondTriangle.vs", "./shaders/secondTriangle.fs");

    firstTriangleShader.clearVertex();
    firstTriangleShader.clearFragment();

    secondTriangleShader.clearVertex();
    secondTriangleShader.clearFragment();

    float firstTriangle[] = {
        //position             
        -0.9f, -0.5f, 0.0f,
        -0.0f, -0.5f, 0.0f,
        -0.45f, 0.5f, 0.0f
    };

    float secondTriangle[] = {
        //position              //color
        0.0f, -0.5f, 0.0f,      1.0f, 0.0f, 0.0f,  // left
        0.9f, -0.5f, 0.0f,      0.0f, 1.0f, 0.0f, // right
        0.45f, 0.5f, 0.0f,      0.0f, 0.0f, 1.0f // top 
    };


    //generate one element buffer object and a vertex array object
    unsigned int VAO[2];
    unsigned int VBO[2];
    //unsigned int EBO;
    
    //generate EBO, VBO and VAO
    //glGenBuffers(1, &EBO);
    glGenBuffers(2, VBO);
    glGenVertexArrays(2, VAO);

    glBindVertexArray(VAO[0]);

    //copy our vertices array in a vertex buffer for OpenGL to use
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(firstTriangle), firstTriangle, GL_STATIC_DRAW);

    //tell OpenGL how it should interpret the vertex data
    //we want to configure the vertex attribute 0
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    //ubind our VAO
    glBindVertexArray(VAO[1]);

    glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(secondTriangle), secondTriangle, GL_STATIC_DRAW);

    //vertices
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    //color
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
  
    while (!glfwWindowShouldClose(window)) {
        //input
        processInput(window);

        //rendering commands
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        //draw our triangle
        firstTriangleShader.use();

        //update ourColor in the fragment shader
        //float timeValue = glfwGetTime();
        //float greenValue = (sin(timeValue) / 2.0f) + 0.5f;
        //int vertexColorLocation = glGetUniformLocation(firstTriangleShader.ID, "ourColor");
        //glUniform4f(vertexColorLocation, 0.0f, greenValue, 0.0f, 1.0f);

        firstTriangleShader.setFloat("horizontalOffset", 0.2f);
        
        //first triangle with VAO[0] and VBO[0]
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        
        secondTriangleShader.use();
        //second triangle with VAO[1] and VBO[1]
        glBindVertexArray(VAO[1]);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        // check and call events and swap the buffers
        glfwPollEvents();
        glfwSwapBuffers(window);
    }

    //de-allocate all resources once they've outlived their purpose
    glDeleteVertexArrays(2, VAO);
    glDeleteBuffers(2, VBO);
    //glDeleteBuffers(1, &EBO);
    firstTriangleShader.clearProgram();
    secondTriangleShader.clearProgram();

    glfwTerminate();
    return 0;
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}