#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shader
{
    public:
        //Shader();
        //reads and build the shader 
        Shader(const char *vertexPath, const char *fragmentPath);

        //use/activate the shader
        void use();

        //clear program
        void clearProgram();
        void clearVertex();
        void clearFragment();

        unsigned int getVertex() const { return vertex; }
        unsigned int getFragment() const { return fragment; }

        //utility uniform functions
        void setBool(const std::string &name, bool value) const;
        void setInt(const std::string &name, int value) const;
        void setFloat(const std::string &name, float value) const;


        //the program ID
        unsigned int ID;

    private:
        unsigned int vertex;
        unsigned int fragment;

};

#endif