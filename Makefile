DIFFERENT_COLOR_BIN = ./bin/different_color

SHADER_BIN = ./bin/shader

DIFFERENT_COLOR_OBJ = 	./obj/glad.o	\
						./obj/Shader.o 	\
						./obj/trianglesDifferentColor.o

SHADERS_OBJ = 	./obj/glad.o	\
				./obj/Shader.o

LIB = -lglfw -lGL -lX11 -lpthread -lXrandr -lXi -ldl

INCLUDE = -I./include

CC = g++

#which binary we want to generate
all:	$(DIFFERENT_COLOR_BIN)

$(DIFFERENT_COLOR_BIN):	$(DIFFERENT_COLOR_OBJ)
	$(CC) $(DIFFERENT_COLOR_OBJ) $(LIB) -o $(DIFFERENT_COLOR_BIN)

$(SHADER_BIN): $(SHADERS_OBJ)
	$(CC) $(SHADERS_OBJ) $(LIB) -o $(SHADER_BIN)


./obj/glad.o: ./src/glad.c
	gcc -c ./src/glad.c $(INCLUDE) -o ./obj/glad.o

./obj/trianglesDifferentColor.o:	./src/trianglesDifferentColor.cpp
	$(CC) -c ./src/trianglesDifferentColor.cpp $(INCLUDE) -o ./obj/trianglesDifferentColor.o

./obj/Shader.o:	./src/Shader.cpp ./src/Shader.h
	$(CC) -c ./src/Shader.cpp $(INCLUDE) -o ./obj/Shader.o


clean:
	rm obj/*

fclean: clean
	rm $(BIN)