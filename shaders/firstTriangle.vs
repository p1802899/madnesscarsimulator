#version 450 core

layout (location = 0) in vec3 aPos;

uniform float horizontalOffset;

out vec3 vertexPosition;

void main()
{
    //exo 1
    gl_Position = vec4(aPos.x + horizontalOffset, -aPos.y, aPos.z, 1.0);
    vertexPosition = gl_Position.xyz;
}