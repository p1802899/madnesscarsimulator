#version 450 core

out vec4 FragColor;
in vec3 vertexPosition;

//uniform vec4 ourColor;


void main()
{
    FragColor = vec4(vertexPosition, 1.0);
}